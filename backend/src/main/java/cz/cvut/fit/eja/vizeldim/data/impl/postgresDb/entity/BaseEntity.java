package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity {
    @Id
    @GeneratedValue
    protected Long id;

    public Long getId() {
        return id;
    }
}
