package cz.cvut.fit.eja.vizeldim.data.layer;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.PurchaseDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;

public interface IPurchaseService extends IEntityService<PurchaseDTO> {
}
