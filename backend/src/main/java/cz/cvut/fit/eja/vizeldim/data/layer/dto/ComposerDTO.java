package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ComposerDTO extends BaseDto {
    public String name;
}
