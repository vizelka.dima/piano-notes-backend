package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.NoteDetail;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.INoteDetailMapper;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository.NoteDetailRepository;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.template.EntityService;
import cz.cvut.fit.eja.vizeldim.data.layer.INoteDetailService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDetailDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.exception.MyNotFoundException;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;


@ApplicationScoped
public class NoteDetailService extends EntityService<NoteDetail, NoteDetailDTO> implements INoteDetailService {
    @Inject
    public NoteDetailService(INoteDetailMapper mapper,
                             NoteDetailRepository noteDetailRepository) {
        this.cl = NoteDetailDTO.class;

        this.mainRepository = noteDetailRepository;
        this.mapper = mapper;
    }

    @Override
    public Uni<NoteDetailDTO> getById(Long id) {
        return mainRepository
                .find("#NoteDetail.findByIdWithDeps", id)
                .firstResult()
                .onItem().ifNull().failWith(new MyNotFoundException())
                .map(mapper::toDto);
    }

    @Override
    public Uni<PageDto<NoteDetailDTO>> getPageByName(Page validPage, String name) {
        return getPageByQuery(validPage,
                "#NoteDetail.searchAllByName",
                "#NoteDetail.countAllByName",
                name);
    }

    @Override
    public Uni<PageDto<NoteDetailDTO>> getPageByCategory(Page page, Long categoryId) {
        return getPageByQuery(page,
                "#NoteDetail.searchByCategory",
                "#NoteDetail.countByCategory",
                categoryId);
    }
}