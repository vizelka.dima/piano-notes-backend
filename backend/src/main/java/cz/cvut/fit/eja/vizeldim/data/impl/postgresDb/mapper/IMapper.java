package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.List;

public interface IMapper<Entity, Dto> {
    @Mapping(target = "id", ignore = true)
    Entity toEntity(Dto dto);

    Dto toDto(Entity entity);

    List<Dto> toDtoList(List<Entity> entityList);

    List<Entity> toEntityList(List<Dto> dtoList);

    @Mapping(target = "id", ignore = true)
    void updateEntity(@MappingTarget Entity entity, Dto dto);

    @Named("WithoutDeps")
    Dto toDtoWithoutDeps(Entity entity);

    @Named("ListWithoutDeps")
    List<Dto> toDtoListWithoutDeps(List<Entity> entityList);
}
