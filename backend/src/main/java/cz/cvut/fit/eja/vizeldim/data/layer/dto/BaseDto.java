package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseDto {
    public Long id;
}
