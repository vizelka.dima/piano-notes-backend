package cz.cvut.fit.eja.vizeldim.data.layer.dto.page;

import java.util.List;

public class PageDto<Dto> {
    public List<Dto> results;

    public Long totalPages;
}
