package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.auth;

import cz.cvut.fit.eja.vizeldim.data.layer.auth.IJwtAuthService;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.JwtToken;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.UserDto;
import io.smallrye.jwt.build.Jwt;

import javax.inject.Singleton;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class MicroprofileJwtAuthService implements IJwtAuthService {
    private String generateAccessToken(UserDto user) {
        return Jwt.issuer("notes-jwt-provider")
                .subject("notes-app-access-token")
                .groups(user.getRoles()
                        .stream().map(Enum::toString)
                        .collect(Collectors.toSet()))
                .expiresAt((System.currentTimeMillis() / 1000) + 15 * 60) // 15 min
                .sign();
    }

    private String generateRefreshToken(UserDto user) {
        return Jwt.issuer("notes-jwt-provider")
                .subject("notes-app-refresh-token")
                .groups(new HashSet<>(List.of("JWT_REFRESHER")))
                .claim("email", user.getEmail())
                .expiresAt((System.currentTimeMillis() / 1000) + 24 * 60 * 60) // 1 day
                .sign();
    }

    @Override
    public JwtToken generate(UserDto user) {
        String accessToken = generateAccessToken(user);
        String refreshToken = generateRefreshToken(user);

        return new JwtToken(accessToken, refreshToken);
    }

    @Override
    public JwtToken refresh(long expirationTime, UserDto userDto, JwtToken jwtDto) {
        jwtDto.accessToken = generateAccessToken(userDto);
        return jwtDto;
    }
}
