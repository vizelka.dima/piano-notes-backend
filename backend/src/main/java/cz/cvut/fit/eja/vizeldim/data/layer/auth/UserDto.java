package cz.cvut.fit.eja.vizeldim.data.layer.auth;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class UserDto {
    private Long id;
    private String email;
    private String password;
    private Set<RoleDto> roles = new HashSet<>();
}
