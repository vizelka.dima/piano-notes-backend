package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.NoteDetail;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.decomposition.CategoryNoteDetail;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.CategoryDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDetailDTO;
import org.mapstruct.*;

import java.util.Collection;
import java.util.List;


@Mapper(componentModel = "CDI", uses = INoteMapper.class, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface INoteDetailMapper extends IMapper<NoteDetail, NoteDetailDTO> {
    @Named("WithoutDeps")
    @Mappings({
            @Mapping(target = "notes", ignore = true),
            @Mapping(target = "categories", ignore = true)
    })
    NoteDetailDTO toDtoWithoutDeps(NoteDetail noteDetail);

    @Override
    @Named("ListWithoutDeps")
    @IterableMapping(qualifiedByName = "WithoutDeps")
    List<NoteDetailDTO> toDtoListWithoutDeps(List<NoteDetail> noteDetails);

    @Override
    @Named("WithDeps")
    @Mapping(source = "categoriesBinding", target = "categories")
    NoteDetailDTO toDto(NoteDetail noteDetail);

    Collection<CategoryDTO> toCategoryDtoList(Collection<CategoryNoteDetail> categoryNoteDetails);

    @Mappings({
            @Mapping(target = "id", source = "categoryNoteDetail.category.id"),
            @Mapping(target = "name", source = "categoryNoteDetail.category.name")
    })
    CategoryDTO toCategoryDto(CategoryNoteDetail categoryNoteDetail);

    @Override
    @Mappings({
            @Mapping(target = "notes", ignore = true),
            @Mapping(target = "categoriesBinding", ignore = true)
    })
    void updateEntity(@MappingTarget NoteDetail noteDetail, NoteDetailDTO noteDetailDTO);
}