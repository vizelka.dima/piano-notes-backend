package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.view.Views;
import io.quarkus.hibernate.reactive.panache.common.ProjectedFieldName;
import io.quarkus.runtime.annotations.RegisterForReflection;

import java.util.List;

@RegisterForReflection
public class NoteDTO extends BaseDto {
    @JsonView({Views.NoteDetail.class})
    public String description;
    @JsonView({Views.NoteDetail.class})
    public String coverUrl;

    @JsonView(Views.Note.class)
    public Long noteDetailId;
    @JsonView({Views.NoteDetail.class})
    public Integer tonality;
    @JsonView({Views.NoteDetail.class})
    public Integer complexity;

    @JsonView({Views.NoteDetail.class})
    public List<PreviewDTO> previews;

    @JsonView({Views.NoteDetail.class})
    public List<FileDTO> files;

    public NoteDTO(Long id,
                   String description,
                   String coverUrl,
                   @ProjectedFieldName("noteDetail.id") Long noteDetailId,
                   Integer tonality,
                   Integer complexity
    ) {
        this.id = id;
        this.description = description;
        this.coverUrl = coverUrl;
        this.noteDetailId = noteDetailId;
        this.tonality = tonality;
        this.complexity = complexity;
    }
}
