package cz.cvut.fit.eja.vizeldim.data.layer.paging;

public class Page {
    private final int page;
    private final int size;

    public Page(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }
}
