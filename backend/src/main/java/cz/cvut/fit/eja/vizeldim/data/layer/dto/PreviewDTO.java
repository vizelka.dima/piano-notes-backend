package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.view.Views;

public class PreviewDTO extends BaseDto {
    @JsonView({Views.NoteDetail.class})
    public final String path;

    @JsonView({Views.NoteDetailPage.class})
    public final Long noteId;

    public PreviewDTO(Long id, String path, Long noteId) {
        this.id = id;
        this.path = path;
        this.noteId = noteId;
    }
}
