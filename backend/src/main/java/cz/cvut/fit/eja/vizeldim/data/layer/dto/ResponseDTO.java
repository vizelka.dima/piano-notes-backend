package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class ResponseDTO extends BaseDto {
    private final String statusCode;
    private final String msg;
}
