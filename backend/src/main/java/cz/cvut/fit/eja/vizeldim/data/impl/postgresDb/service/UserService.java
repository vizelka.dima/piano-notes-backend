package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.IUserMapper;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository.UserRepository;
import cz.cvut.fit.eja.vizeldim.data.layer.IUserService;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.AuthDto;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.UserDto;
import cz.cvut.fit.eja.vizeldim.data.layer.exception.MyNotFoundException;
import io.quarkus.elytron.security.common.BcryptUtil;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class UserService implements IUserService {
    @Inject
    UserRepository userRepository;

    @Inject
    IUserMapper userMapper;

    @Override
    public Uni<UserDto> getByEmail(String email) {
        return userRepository
                .find("#MyUser.FindByEmail", email)
                .firstResult()
                .onItem().ifNull().failWith(new MyNotFoundException())
                .map(userMapper::toDto);
    }

    @Override
    public Uni<UserDto> verify(AuthDto authDto) {
        return getByEmail(authDto.email)
                .onItem().ifNotNull().transform(u -> {
                    boolean isAuth = BcryptUtil.matches(authDto.password, u.getPassword());
                    return isAuth ? u : null;
                });
    }
}

