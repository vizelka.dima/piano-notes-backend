package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.template;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.IMapper;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.exception.MyDbException;
import cz.cvut.fit.eja.vizeldim.data.layer.exception.MyNotFoundException;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.PageSettings;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;
import io.quarkus.hibernate.reactive.panache.PanacheQuery;
import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;

import java.util.List;

public abstract class EntityService<Entity, Dto> implements IEntityService<Dto> {
    protected IMapper<Entity, Dto> mapper;
    protected PageSettings pageSettings = new PageSettings();
    protected Class<Dto> cl;
    protected PanacheRepository<Entity> mainRepository;

    protected Uni<Entity> mapRelations(Entity entity, Dto dto) {
        return Uni.createFrom().item(entity);
    }

    protected Uni<Entity> addDeps(Uni<Entity> entityUni) {
        return entityUni;
    }

    @Override
    public Uni<List<Dto>> getList(Page page) {
        page = pageSettings.transform(page);

        return mainRepository
                .findAll()
                .page(page.getPage() - 1, page.getSize())
                .project(cl)
                .list();
    }

    @Override
    public Uni<PageDto<Dto>> getPage(Page page) {
        page = pageSettings.transform(page);

        PanacheQuery<Entity> query = mainRepository
                .findAll()
                .page(page.getPage() - 1, page.getSize());

        PageDto<Dto> res = new PageDto<>();
        return query
                .pageCount().invoke(x -> res.totalPages = x.longValue())
                .replaceWith(query.project(cl).list())
                .map(p -> {
                    res.results = p;
                    return res;
                });
    }


    @Override
    public Uni<Dto> getByIdWithoutListDeps(Long id) {
        return mainRepository
                .findById(id)
                .onItem().ifNull().failWith(MyNotFoundException::new)
                .map(mapper::toDtoWithoutDeps);
    }

    @Override
    public Uni<Dto> getById(Long id) {
        return addDeps(mainRepository
                .findById(id)
                .onItem().ifNull().failWith(MyNotFoundException::new))
                .map(mapper::toDto); //ListDeps
    }

    @Override
    @ReactiveTransactional
    public Uni<Dto> save(Dto createDto) {
        Entity e = mapper.toEntity(createDto);
        return mapRelations(e, createDto)
                .onItem().ifNotNull().call(mainRepository::persist)
                .onFailure().transform(x -> new MyDbException())
                .map(mapper::toDto);
    }

    @Override
    @ReactiveTransactional
    public Uni<Dto> update(long id, Dto updateDto) {
        return mainRepository
                .findById(id)
                .onItem().ifNull().failWith(MyNotFoundException::new)
                .map(found -> {
                    mapper.updateEntity(found, updateDto);
                    return found;
                })
                .call(mainRepository::persist)
                .map(mapper::toDtoWithoutDeps);
    }

    protected Uni<List<Dto>> getListByQuery(Page page, String query, Object... params) {
        page = pageSettings.transform(page);

        return mainRepository
                .find(query, params)
                .page(page.getPage() - 1, page.getSize())
                .list()
                .map(mapper::toDtoListWithoutDeps);
    }

    protected Uni<PageDto<Dto>> getPageByQuery(Page page,
                                               String findQuery,
                                               String countQuery,
                                               Object... params) {
        page = pageSettings.transform(page);

        PanacheQuery<Entity> fq = mainRepository
                .find(findQuery, params)
                .page(page.getPage() - 1, page.getSize());

        PageDto<Dto> res = new PageDto<>();

        long pageSize = page.getSize();
        return mainRepository
                .count(countQuery, params).invoke(x -> res.totalPages = (x == 0) ? 0 : ((x / pageSize) + 1))
                .replaceWith(fq.list())
                .map(mapper::toDtoListWithoutDeps)
                .map(p -> {
                    res.results = p;
                    return res;
                });
    }

}
