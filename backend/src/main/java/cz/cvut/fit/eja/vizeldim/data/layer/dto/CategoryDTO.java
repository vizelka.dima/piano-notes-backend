package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

import javax.validation.constraints.NotNull;

@RegisterForReflection
public class CategoryDTO extends BaseDto {
    @NotNull
    public final String name;

    public CategoryDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
