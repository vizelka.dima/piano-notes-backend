package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Purchase;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.PurchaseDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface IPurchaseMapper extends IMapper<Purchase, PurchaseDTO> {
}
