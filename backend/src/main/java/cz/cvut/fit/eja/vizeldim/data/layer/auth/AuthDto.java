package cz.cvut.fit.eja.vizeldim.data.layer.auth;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AuthDto {
    @NotNull
    public String email;
    @NotNull
    public String password;
}
