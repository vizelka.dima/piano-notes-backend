package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.File;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.FileDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "CDI")
public interface IFileMapper extends IMapper<File, FileDTO> {
    @Override
    @Mapping(source = "note.id", target = "noteId")
    FileDTO toDto(File note);
}
