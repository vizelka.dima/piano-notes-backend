package cz.cvut.fit.eja.vizeldim.data.layer.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.view.Views;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
public class FileDTO extends BaseDto {
    @JsonView({Views.NoteDetailPage.class})
    public final String name;

    @JsonView({Views.NoteDetail.class})
    public final BigDecimal price;

    @JsonView({Views.NoteDetailPage.class})
    public final Boolean published;

    @JsonView({Views.NoteDetail.class})
    public final String format;

    @JsonView({Views.NoteDetailPage.class})
    public final Long noteId;
}
