package cz.cvut.fit.eja.vizeldim.resource;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.eja.vizeldim.data.layer.INoteDetailService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDetailDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.view.Views;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import cz.cvut.fit.eja.vizeldim.resource.template.EntityResource;
import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.reactive.RestResponse;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.UriInfo;


@Path("notedetails")
public class NoteDetailResource extends EntityResource<NoteDetailDTO> {
    private final INoteDetailService noteDetailDao;


    @Inject
    public NoteDetailResource(INoteDetailService noteDetailDao) {
        super(noteDetailDao, NoteDetailResource.class);
        this.noteDetailDao = noteDetailDao;
    }

    @Override
    @JsonView(Views.NoteDetail.class)
    public Uni<RestResponse<NoteDetailDTO>> getById(Long id) {
        return super.getById(id);
    }

    @Override
    @JsonView(Views.NoteDetailPage.class)
    public Uni<RestResponse<PageDto<NoteDetailDTO>>> getPage(int page, int size, UriInfo uriInfo) {
        String name = uriInfo.getQueryParameters().getFirst("name");
        String category = uriInfo.getQueryParameters().getFirst("category");

        Page _page = new Page(page, size);

        if (category != null) {
            long _category = Long.parseLong(category);
            return noteDetailDao
                    .getPageByCategory(_page, _category)
                    .map(RestResponse::ok);
        }

        if (name != null) {
            return noteDetailDao
                    .getPageByName(_page, name)
                    .map(RestResponse::ok);
        }

        return super.getPage(page, size, null);
    }
}
