package cz.cvut.fit.eja.vizeldim.data.layer;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.CategoryDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import cz.cvut.fit.eja.vizeldim.data.layer.template.IEntityService;
import io.smallrye.mutiny.Uni;

public interface ICategoryService extends IEntityService<CategoryDTO> {
    Uni<PageDto<CategoryDTO>> getPageByName(Page validPage, String name);
}
