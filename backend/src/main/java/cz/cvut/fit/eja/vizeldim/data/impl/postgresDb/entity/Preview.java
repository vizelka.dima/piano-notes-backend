package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Preview extends BaseEntity {
    private String path;

    @ManyToOne
    @JoinColumn(name = "note_id")
    private Note note;
}
