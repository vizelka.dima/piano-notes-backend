package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Category;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.mapper.ICategoryMapper;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.repository.CategoryRepository;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.service.template.EntityService;
import cz.cvut.fit.eja.vizeldim.data.layer.ICategoryService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.CategoryDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;


@ApplicationScoped
public class CategoryService extends EntityService<Category, CategoryDTO> implements ICategoryService {

    @Inject
    public CategoryService(ICategoryMapper mapper,
                           CategoryRepository categoryRepository) {
        this.cl = CategoryDTO.class;
        this.mainRepository = categoryRepository;
        this.mapper = mapper;
    }

    @Override
    public Uni<PageDto<CategoryDTO>> getPageByName(Page validPage, String name) {
        return getPageByQuery(validPage,
                "#Category.searchAllByName",
                "#Category.countAllByName",
                name);
    }
}
