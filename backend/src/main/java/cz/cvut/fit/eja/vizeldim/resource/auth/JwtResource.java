package cz.cvut.fit.eja.vizeldim.resource.auth;

import cz.cvut.fit.eja.vizeldim.data.layer.IUserService;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.AuthDto;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.IJwtAuthService;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.JwtToken;
import cz.cvut.fit.eja.vizeldim.data.layer.auth.UserDto;
import io.quarkus.security.identity.CurrentIdentityAssociation;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

@Produces(MediaType.APPLICATION_JSON)
@Path("/auth")
@RequestScoped
public class JwtResource {
    // @Inject JsonWebToken jwt; Accessing this would be Blocking
    // @Blocking would be automatically set if quarkus.http.auth.proactive=true
    // Non-blocking approach
    @Inject
    CurrentIdentityAssociation identities;

    private Uni<JsonWebToken> getJwt() {
        return identities.getDeferredIdentity()
                .onItem()
                .transform(identity -> (JsonWebToken) identity.getPrincipal());
    }

    @Inject
    IJwtAuthService authService;

    @Inject
    IUserService userService;


    @POST
    @Path("jwt")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Uni<Response> getJwt(@Valid @NotNull AuthDto authRequest) {
        Uni<UserDto> isAuth = userService.verify(authRequest);

        return isAuth
                .onItem().ifNotNull()
                .transform(u -> {
                    JwtToken jwtDto = authService.generate(u);
                    NewCookie refreshTokenCookie = new NewCookie(
                            "jwt-refresh",
                            jwtDto.refreshToken,
                            null, null, 1, null, 24 * 60 * 60, null, true, true);

                    return Response.ok(jwtDto).cookie(refreshTokenCookie).build();
                })
                .onItem().ifNull()
                .continueWith(Response.status(Response.Status.FORBIDDEN).build());

//        curl -i -d '{"email": "admin", "password": "pass"}' -H 'Content-Type: application/json' -X POST "http://localhost:8081/jwt"
    }


    @GET
    @Path("refresh")
    @RolesAllowed({"JWT_REFRESHER"})
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> refreshJwt() {
        return getJwt().chain(jwt -> {
            long time = jwt.getExpirationTime();
            String email = jwt.getClaim("email");
            JwtToken jwtDto = new JwtToken();
            jwtDto.refreshToken = jwt.getRawToken();

            NewCookie refreshTokenCookie = new NewCookie(
                    "Bearer",
                    jwtDto.refreshToken,
                    null, null, 1, null, 24 * 60 * 60, null, true, true);

            return userService.getByEmail(email)
                    .map(user -> authService.refresh(time, user, jwtDto))
                    .map(accessToken -> Response.ok(accessToken).cookie(refreshTokenCookie).build());
        });
    }

    @GET
    @Path("test")
    @RolesAllowed({"ADMIN"})
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> testAdminRoute() {
        return Uni.createFrom().item(Response.ok("{\"msg\": \"HELLO, ADMIN :)\"}").build());
    }
}
