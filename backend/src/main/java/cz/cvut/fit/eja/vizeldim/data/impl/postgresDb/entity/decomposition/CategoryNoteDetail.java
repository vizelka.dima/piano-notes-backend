package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.decomposition;

import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.Category;
import cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity.NoteDetail;

import javax.persistence.*;


@Entity
public class CategoryNoteDetail {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    public Category category;

    @ManyToOne
    @JoinColumn(name = "note_detail_id")
    public NoteDetail noteDetail;
}
