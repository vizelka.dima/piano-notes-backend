package cz.cvut.fit.eja.vizeldim.data.impl.postgresDb.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Collections;
import java.util.Set;

@Entity
@Getter
@Setter
public class Note extends BaseEntity {
    String description;
    String coverUrl;

    @ManyToOne
    @JoinColumn(name = "note_detail_id")
    NoteDetail noteDetail;

    Integer tonality;

    Integer complexity;

    @OneToMany(mappedBy = "note")
    Set<File> files = Collections.emptySet();

    @OneToMany(mappedBy = "note")
    Set<Preview> previews = Collections.emptySet();

    public void addFile(File file) {
        this.files.add(file);
    }

    public void addPreview(Preview preview) {
        this.previews.add(preview);
    }
}
