package cz.cvut.fit.eja.vizeldim.resource;

import cz.cvut.fit.eja.vizeldim.data.layer.INoteService;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.FileDTO;
import cz.cvut.fit.eja.vizeldim.data.layer.dto.NoteDTO;
import cz.cvut.fit.eja.vizeldim.resource.template.EntityResource;
import io.smallrye.mutiny.Uni;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("notes")
public class NoteResource extends EntityResource<NoteDTO> {
    private final INoteService noteDao;

    @Inject
    public NoteResource(INoteService noteDao) {
        super(noteDao, NoteResource.class);
        this.noteDao = noteDao;
    }


    @POST
    @Path("/{id}/files")
    @RolesAllowed({"ADMIN"})
    public Uni<Response> addFile(@PathParam("id") long id,
                                 FileDTO fileDTO) {
        return Uni.createFrom().item(Response.noContent().build());
    }
}
