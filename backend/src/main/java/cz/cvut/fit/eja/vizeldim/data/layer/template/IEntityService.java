package cz.cvut.fit.eja.vizeldim.data.layer.template;

import cz.cvut.fit.eja.vizeldim.data.layer.dto.page.PageDto;
import cz.cvut.fit.eja.vizeldim.data.layer.paging.Page;
import io.smallrye.mutiny.Uni;

import java.util.List;

public interface IEntityService<Dto> {
    Uni<Dto> getById(Long id);

    Uni<Dto> getByIdWithoutListDeps(Long id);

    Uni<List<Dto>> getList(Page page);

    Uni<PageDto<Dto>> getPage(Page page);

    Uni<Dto> save(Dto entity);

    Uni<Dto> update(long id, Dto entity);
}
