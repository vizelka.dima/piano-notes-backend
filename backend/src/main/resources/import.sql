INSERT INTO Category(id, name) VALUES(nextval('hibernate_sequence'), 'Classical');
INSERT INTO Category(id, name) VALUES(nextval('hibernate_sequence'), 'Jazz');
INSERT INTO Category(id, name) VALUES(nextval('hibernate_sequence'), 'Movie songs');

INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 1', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 2', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 3', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 4', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 5', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 6', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 7', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 8', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 9', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 10', 'preview.png', 'youtube.com/orig#url', 1);
INSERT INTO NoteDetail(id, name, preview, originalUrl, originalTonality) VALUES(nextval('hibernate_sequence'), 'Note Detail 11', 'preview.png', 'youtube.com/orig#url', 1);

INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 1', 'http://https://www.youtube.com/', 4, 1, 1);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 2', 'http://https://www.youtube.com/', 4, 1, 2);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 3', 'http://https://www.youtube.com/', 4, 2, 1);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 4', 'http://https://www.youtube.com/', 4, 3, 2);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 5', 'http://https://www.youtube.com/', 7, 1, 1);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 6', 'http://https://www.youtube.com/', 6, 1, 1);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 7', 'http://https://www.youtube.com/', 6, 1, 1);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 8', 'http://https://www.youtube.com/', 5, 1, 1);
INSERT INTO Note(id, description, coverUrl, note_detail_id, tonality, complexity) VALUES(nextval('hibernate_sequence'), 'DESC 9', 'http://https://www.youtube.com/', 5, 1, 1);


INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 1, 4);
INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 1, 6);
INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 1, 7);

INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 2, 4);
INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 2, 5);
INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 2, 6);
INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 2, 7);

INSERT INTO CategoryNoteDetail(id, category_id, note_detail_id) VALUES(nextval('hibernate_sequence'), 3, 8);

INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 15);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview2.png', 16);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 17);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 18);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 19);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 20);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 21);
INSERT INTO Preview(id, path, note_id) VALUES(nextval('hibernate_sequence'), 'preview.png', 22);

INSERT INTO File(id, name, price, published, format, note_id) VALUES(nextval('hibernate_sequence'), 'reactive-upload644554656', 7, true, 'pdf', 15);
INSERT INTO File(id, name, price, published, format, note_id) VALUES(nextval('hibernate_sequence'), 'reactive-upload644554656', 3, true, 'midi', 15);


INSERT INTO my_user(id, email, password) VALUES(nextval('hibernate_sequence'), 'admin', '$2a$10$4s83Ud7U9W/rIhR5ZMEkCeOWI1LPsyqdfIch4OnJSLqr1FhzRUPoK'); -- admin, pass
INSERT INTO my_user_role(my_user_id, role) VALUES(42, 'ADMIN');
